#!/bin/ash
###
#
# Author: Maël
# Date: 2022/01/19
# Desc:
#   - Setup Wireguard Interface and add peer
#
###

function main() {

  opkg update && opkg install wireguard-tools luci-app-wireguard

  while [[ -z $WG_NAME ]]; do
    echo "---"
    read -p "Wireguard interface name: [wg0]" WG_NAME
    WG_NAME=${WG_NAME:-"wg0"}
  done

  while [[ -z $WG_IP ]]; do
    echo "---"
    read -p "Wireguard Interface IP: " WG_IP
  done

  while [[ -z $WG_PORT ]]; do
    echo "---"
    read -p "Wireguard interface port [51820]: " WG_PORT
    WG_PORT=${WG_PORT:-"51820"}
  done

  while [[ -z $WG_PATH ]]; do
    echo "---"
    read -p "Where keys will be storred: [/etc/wireguard]" WG_PATH
    WG_PATH=${WG_PATH:-"/etc/wireguard"}
  done

  if [ ! -d $WG_PATH ]; then
    mkdir -m 700 $WG_PATH
  fi

  wg genkey | tee $WG_PATH/$WG_NAME.key | wg pubkey > $WG_PATH/$WG_NAME.pub
  chmod 600 $WG_PATH/*

  echo -e "
  ############################################
  Public key:
  $(cat $WG_PATH/$WG_NAME.pub)
  ############################################
  "

  while [[ -z $PEER_NAME ]]; do
    echo "---"
    read -p "Peer Name: " PEER_NAME
  done

  while [[ -z $PEER_PUBKEY ]]; do
    echo "---"
    read -p "Peer Public key: " PEER_PUBKEY
  done

  while [[ -z $PEER_ALLOWED_IPS ]]; do
    echo "---"
    read -p "Peer Allowed IPs [CIDR Notation]: " PEER_ALLOWED_IPS
  done

  while [[ -z $PEER_ENDPOINT ]]; do
    echo "---"
    read -p "Peer endpoint (fqdn possible as well): " PEER_ENDPOINT
  done

  while [[ -z $PEER_PORT ]]; do
    echo "---"
    read -p "Peer port [51820]: " PEER_PORT
    PEER_PORT=${PEER_PORT:-"51820"}
  done

  while [[ -z $PEER_KEEPALIVE ]]; do
    echo "---"
    read -p "Persistent keepalive [25]: " PEER_KEEPALIVE
    PEER_KEEPALIVE=${PEER_KEEPALIVE:-"25"}
  done

  echo -e "
  ############################################
  OpenWRT will be configured with the following parameters:

  Wireguard interface Name  : $WG_NAME
  Wireguard interface IP    : $WG_IP
  Wireguard listening PORT  : $WG_PORT
  Wireguard keys folder     : $WG_PATH
  Peer name                 : $PEER_NAME
  Peer public key           : $PEER_PUBKEY
  Peer allowed IPs          : $PEER_ALLOWED_IPS
  Peer endpoint address     : $PEER_ENDPOINT
  Peer port                 : $PEER_PORT
  Peer persistent keepalive : $PEER_KEEPALIVE

  ############################################"

  echo "  1 - Continue"
  echo "  2 - Exit"
  read -p "  Your choice [Default 1]: " CONTINUE

  case $CONTINUE in
  1) setup;;
  2) echo "  Exiting..." ; exit 0 ;;
  *) setup;; 
  esac
}

function setup() {

  uci set network.$WG_NAME=interface
  uci set network.$WG_NAME.proto='wireguard'
  uci set network.$WG_NAME.addresses="$WG_IP"
  uci set network.$WG_NAME.listen_port="$WG_PORT"
  uci set network.$WG_NAME.private_key="$(cat $WG_PATH/$WG_NAME.key)"

  WG_NETWORK_ID=$(uci add network wireguard_$WG_NAME)

  uci set network.$WG_NETWORK_ID.description="$PEER_NAME"
  uci set network.$WG_NETWORK_ID.public_key="$PEER_PUBKEY"
  uci set network.$WG_NETWORK_ID.preshared_key="$PRESHARED_KEY"
  uci add_list network.$WG_NETWORK_ID.allowed_ips="$PEER_ALLOWED_IPS"
  uci set network.$WG_NETWORK_ID.route_allowed_ips='1'
  uci set network.$WG_NETWORK_ID.endpoint_host="$PEER_ENDPOINT"
  uci set network.$WG_NETWORK_ID.endpoint_port="$PEER_PORT"
  uci set network.$WG_NETWORK_ID.persistent_keepalive="$PEER_KEEPALIVE"

  ZONE_INDEX=$(uci show firewall | grep zone | grep "name='lan'" | cut -d "." -f 2)
  uci add_list firewall.$ZONE_INDEX.network="$WG_NAME"

  RULE_ID=$(uci add firewall rule)

  uci set firewall.$RULE_ID.name="$WG_NAME-Ingoing"
  uci set firewall.$RULE_ID.family='ipv4'
  uci add_list firewall.$RULE_ID.proto='udp'
  uci set firewall.$RULE_ID.src='wan'
  uci set firewall.$RULE_ID.dest_port="$WG_PORT"
  uci set firewall.$RULE_ID.target='ACCEPT'

  echo -e "
############################################
  Setup done.

 $(uci changes)
############################################"

  read -p "Commit changes and reboot ? [Y/n]: " COMMIT

  case $COMMIT in
    y) uci commit && echo "--> Reboot in 7sec..." && sleep 7 && reboot ;;
    n) echo "Exiting..." && exit 0 ;;
    *) uci commit && echo "--> Reboot in 7sec..." && sleep 7 && reboot ;;
  esac
}
main
