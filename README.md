## Wireguard Site to Site

### Context Example:

```console
Site1(192.168.101.0/24) <---> WRT1(PublicIP: 1.1.1.1) <== WAN ==> (PublicIP: 1.1.1.2)WRT2 <---> Site2(192.168.102.0/24)
```

### Output Example:
```console
root@wrt1:~# wget https://gitlab.com/snax44/openwrt_wireguard-s2s/-/raw/main/setup.sh
Downloading 'https://gitlab.com/snax44/openwrt_wireguard-s2s/-/raw/main/setup.sh'
Connecting to 172.65.251.78:443
Writing to 'setup.sh'
setup.sh             100% |*******************************|  4255   0:00:00 ETA
Download completed (4255 bytes)
root@wrt1:~# ash setup.sh 
Downloading https://downloads.openwrt.org/releases/21.02.1/targets/x86/64/packages/Packages.gz
Updated list of available packages in /var/opkg-lists/openwrt_core
[...]
Installing luci-proto-wireguard (git-21.243.21928-71fe35c) to root...
Downloading https://downloads.openwrt.org/releases/21.02.1/packages/x86_64/luci/luci-proto-wireguard_git-21.243.21928-71fe35c_all.ipk
Configuring kmod-crypto-hash.
Configuring kmod-crypto-lib-blake2s.
Configuring kmod-crypto-lib-chacha20.
Configuring kmod-crypto-lib-poly1305.
Configuring kmod-crypto-lib-chacha20poly1305.
Configuring kmod-crypto-kpp.
Configuring kmod-crypto-lib-curve25519.
Configuring kmod-udptunnel4.
Configuring kmod-udptunnel6.
Configuring kmod-wireguard.
Configuring wireguard-tools.
Configuring luci-proto-wireguard.
Configuring luci-app-wireguard.
---
Wireguard interface name: [wg0]
---
Wireguard Interface IP: 192.168.101.2
---
Wireguard interface port [51820]: 55555
---
Where keys will be storred: [/etc/wireguard]

  ############################################
  Public key:
  65r+M50LhwIvX3drJnQNjTi7bqg9yU6TZwlNXLKu4mk=
  ############################################
  
---
Peer Name: wrt2 
---
Peer Public key: <PUB_KEY_FROM_WRT2>        
---
Peer Allowed IPs [CIDR Notation]: 192.168.102.0/24
---
Peer endpoint (fqdn possible as well): 1.1.1.2
---
Peer port [51820]: 55555
---
Persistent keepalive [25]: 

  ############################################
  OpenWRT will be configured with the following parameters:

  Wireguard interface Name  : wg0
  Wireguard interface IP    : 192.168.101.2
  Wireguard listening PORT  : 55555
  Wireguard keys folder     : /etc/wireguard
  Peer name                 : wrt2
  Peer public key           : <PUB_KEY_FROM_WRT2>
  Peer allowed IPs          : 192.168.102.0/24
  Peer endpoint address     : 1.1.1.2
  Peer port                 : 55555
  Peer persistent keepalive : 25

  ############################################
  1 - Continue
  2 - Exit
  Your choice [Default 1]:

  ############################################
  Setup done.

  firewall.cfg02dc81.network+='wg0'
firewall.cfg1592bd='rule'
firewall.cfg1592bd.name='wg0-Ingoing'
firewall.cfg1592bd.family='ipv4'
firewall.cfg1592bd.proto+='udp'
firewall.cfg1592bd.src='wan'
firewall.cfg1592bd.dest_port='55555'
firewall.cfg1592bd.target='ACCEPT'
network.wg0='interface'
network.wg0.private_key='ADWdr4tMjxpkBuwFqaQBUh5/MgqxgUgveNhEbvS0j0M='
network.cfg0c96fc='wireguard_wg0'
network.cfg0c96fc.description='wrt1'
network.cfg0c96fc.public_key='<PUB_KEY_FROM_WRT2>'
network.cfg0c96fc.allowed_ips+='1'
network.cfg0c96fc.route_allowed_ips='192.168.102.0/24'
network.cfg0c96fc.endpoint_host='1.1.1.2'
network.cfg0c96fc.endpoint_port='55555'
network.cfg0c96fc.persistent_keepalive='25'
  ############################################
Commit changes and reboot ? [Y/n]:
--> Reboot in 7sec...
